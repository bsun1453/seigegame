# README #

# prog2370final #
Final project for the PROG2370: Game Programming with Data Structures course.
The project involves making a 2D or 3D game using the Monogame framework.
My game is a 3D isometric shooter.

Click on the Project folder to view the .cs files.

This was programmed in Visual Studio.
The code shown here are the functional code from the game.
Due to potential copyright issues game assets such as models, textures, music and soundeffects are not uploaded.

NOTE: The code is not optimized nor refactored, due to time constraints and lack of experience in utilizing the Monogame framework. However, all project requirements were met or exceeded.

### Requirements ###
  
[Monogame Framework](https://www.monogame.net/downloads/)  
[Visual Studio 2017](https://visualstudio.microsoft.com/vs/older-downloads/) or better  
Proficient enough to compile and run the project  

### License ###
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  
  
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  
  
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.  
  
**Note:** I chose to license this under GPLv3 because I do not care how the assets of this project is used provided it is not for financial gain, which is why the GPLv3 with it's strong copyleft protection is a suitable choice in license.
  
### Images: ###
Gameplay screenshot showing game objective and the destructability of the wooden walls.  
  
![gameplay screenshot](https://bzys.github.io/portfolio/images/monoGame/seigeGameplay.JPG)  
  
In-game help file description of controls and gameplay mechanics.
  
![help menu screenshot](https://bzys.github.io/portfolio/images/monoGame/seigeHelp.JPG)  

